//
//  ViewController.swift
//  StarAnimationDemo
//
//  Created by HamGuy on 26/11/2016.
//  Copyright © 2016 HamGuy. All rights reserved.
//

import Cocoa
import  SceneKit
import SpriteKit

class ViewController: NSViewController {

    @IBOutlet weak var skView:SCNView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let scene = SCNScene()
        let particlesNode = SCNNode()
        let particleSystem = SCNParticleSystem(named: "Welcome", inDirectory: "")
        particlesNode.addParticleSystem(particleSystem!)
        scene.rootNode.addChildNode(particlesNode)
        skView.backgroundColor = .black
        skView.scene = scene
        
        particlesNode.position = SCNVector3(0, 0, -20)
    }

    override var representedObject: Any? {
        didSet {
        // Update the view, if already loaded.
        }
    }


}

